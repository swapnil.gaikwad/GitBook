# My AEM Questions

8.The dispatcher will NOT cache a document if what circumstance is true?

Ans:- The HTTP method is GET.

Explanation :- What does the dispatcher cache?

1. The URL must be allowed by the

[/cache](tg://bot_command?command=cache)

rules and

[/filter](tg://bot_command?command=filter)

sections of dispatcher.any

1. The URL must have a file extension “/content/foo.html - cached” and “/content/foo not cached ”

2. The URL must not contain any query string parameter “/content/foo.html cached”

“/content/foo.html?queryparam=1 not cached ”

1. If the URL has a suffix, then that suffix must have a file extension

“/content/foo.html/suffix/path.html cached” and “/content/foo.html/suffix/path not cached ”

5.The HTTP method must be GET “GET

[/foo](tg://bot_command?command=foo)

.html cached ” and “HEAD

[/foo](tg://bot_command?command=foo)

.html not cached ” and “POST

[/foo](tg://bot_command?command=foo)

.html not cached ”

1. The HTTP response status must be 200 OK “HTTP/1.1 200 OK cached” and “HTTP/1.1 500 Internal Server Error not cached ” and “HTTP/1.1 404 Not Found not cached ”



